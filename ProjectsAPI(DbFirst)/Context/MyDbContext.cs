﻿using Microsoft.EntityFrameworkCore;
using ProjectsAPI_DbFirst_.Models;
using Task = ProjectsAPI_DbFirst_.Models.Task;

namespace ProjectsAPI_DbFirst_.Context;

public partial class MyDbContext : DbContext
{
    public MyDbContext()
    {
    }

    public MyDbContext(DbContextOptions<MyDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Client> Clients { get; set; }

    public virtual DbSet<Employee> Employees { get; set; }

    public virtual DbSet<Project> Projects { get; set; }

    public virtual DbSet<Task> Tasks { get; set; }

    public virtual DbSet<Team> Teams { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=ProjectsApiDb;Trusted_Connection=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Employee>(entity =>
        {
            entity.HasIndex(e => e.TeamId, "IX_Employees_TeamId");

            entity.HasOne(d => d.Team).WithMany(p => p.Employees).HasForeignKey(d => d.TeamId);
        });

        modelBuilder.Entity<Project>(entity =>
        {
            entity.HasIndex(e => e.ClientId, "IX_Projects_ClientId");

            entity.HasIndex(e => e.TeamId, "IX_Projects_TeamId");

            entity.HasOne(d => d.Client).WithMany(p => p.Projects).HasForeignKey(d => d.ClientId);

            entity.HasOne(d => d.Team).WithMany(p => p.Projects).HasForeignKey(d => d.TeamId);
        });

        modelBuilder.Entity<Task>(entity =>
        {
            entity.HasIndex(e => e.AssignedEmployeeId, "IX_Tasks_AssignedEmployeeId");

            entity.HasIndex(e => e.AssignedProjectId, "IX_Tasks_AssignedProjectId");

            entity.HasOne(d => d.AssignedEmployee).WithMany(p => p.Tasks)
                .HasForeignKey(d => d.AssignedEmployeeId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(d => d.AssignedProject).WithMany(p => p.Tasks)
                .HasForeignKey(d => d.AssignedProjectId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
