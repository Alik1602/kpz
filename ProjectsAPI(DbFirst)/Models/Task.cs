﻿using System;
using System.Collections.Generic;

namespace ProjectsAPI_DbFirst_.Models;

public partial class Task
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public DateTime DueDate { get; set; }

    public string Status { get; set; } = null!;

    public int AssignedEmployeeId { get; set; }

    public int AssignedProjectId { get; set; }

    public virtual Employee AssignedEmployee { get; set; } = null!;

    public virtual Project AssignedProject { get; set; } = null!;
}
