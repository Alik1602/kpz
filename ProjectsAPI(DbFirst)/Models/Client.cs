﻿using System;
using System.Collections.Generic;

namespace ProjectsAPI_DbFirst_.Models;

public partial class Client
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string ContactPerson { get; set; } = null!;

    public string Email { get; set; } = null!;

    public string PhoneNumber { get; set; } = null!;

    public virtual ICollection<Project> Projects { get; set; } = new List<Project>();
}
