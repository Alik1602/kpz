﻿using System;
using System.Collections.Generic;

namespace ProjectsAPI_DbFirst_.Models;

public partial class Employee
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int Age { get; set; }

    public string Level { get; set; } = null!;

    public string Role { get; set; } = null!;

    public int TeamId { get; set; }

    public virtual ICollection<Task> Tasks { get; set; } = new List<Task>();

    public virtual Team Team { get; set; } = null!;
}
