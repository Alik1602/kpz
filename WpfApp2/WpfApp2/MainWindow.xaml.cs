﻿using System.Windows;
using WpfApp2.Dialogs;
using WpfApp2.ViewModels;

namespace WpfApp2 {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private TeamViewModel _teamViewModel;
        private ClientViewModel _clientViewModel;

        public MainWindow()
        {
            InitializeComponent();
            _teamViewModel = new TeamViewModel();
            _clientViewModel = new ClientViewModel();
            TeamsDataGrid.ItemsSource = _teamViewModel.Teams;
            ClientsDataGrid.ItemsSource = _clientViewModel.Clients;
            _teamViewModel.GetCommand.Execute(null);
            _clientViewModel.GetCommand.Execute(null);
        }

        private void AddTeamButton_Click(object sender, RoutedEventArgs e)
        {
            _teamViewModel.AddCommand.Execute(null);
        }
        
        private void DeleteTeamButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new TeamDialog();
            if (dialog.ShowDialog() == true)
            {
                _teamViewModel.SelectedTeamId = dialog.TeamId;
                _teamViewModel.DeleteCommand.Execute(null);
            }
        }
        
        private void UpdateTeamButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new TeamDialog();
            if (dialog.ShowDialog() == true)
            {
                _teamViewModel.SelectedTeamId = dialog.TeamId;
                _teamViewModel.UpdateCommand.Execute(null);
            }
        }

        private void AddClientButton_Click(object sender, RoutedEventArgs e)
        {
            _clientViewModel.AddCommand.Execute(null);
        }

        private void DeleteClientButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new ClientDialog();
            if (dialog.ShowDialog() == true)
            {
                _clientViewModel.SelectedClientId = dialog.ClientId;
                _clientViewModel.DeleteCommand.Execute(null);
            }
        }

        private void UpdateClientButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new ClientDialog();
            if (dialog.ShowDialog() == true)
            {
                _clientViewModel.SelectedClientId = dialog.ClientId;
                _clientViewModel.UpdateCommand.Execute(null);
            }
        }
    }
}