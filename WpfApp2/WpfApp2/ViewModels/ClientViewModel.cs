﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using Newtonsoft.Json;
using WpfApp2.Models;

namespace WpfApp2.ViewModels; 

public class ClientViewModel {
    public ObservableCollection<Client>? Clients { get; set; }
    public int SelectedClientId { get; set; }
    public RelayCommand GetCommand { get; set; }

    public ICommand AddCommand { get; set; }
    public ICommand DeleteCommand { get; set; }
    public ICommand UpdateCommand { get; set; }
    private readonly HttpClient _httpClient;
    private const string BaseUrl = "http://localhost:5117/api/clients";

    public ClientViewModel()
    {
        _httpClient = new HttpClient();
        Clients = new ObservableCollection<Client>();
        GetCommand = new RelayCommand(GetClients);
        AddCommand = new RelayCommand(AddClient);
        DeleteCommand = new RelayCommand(DeleteClient);
        UpdateCommand = new RelayCommand(UpdateClient);
    }

    public async void GetClients()
    {
        var response = await _httpClient.GetAsync(BaseUrl);
        if (response.IsSuccessStatusCode)
        {
            var content = await response.Content.ReadAsStringAsync();
            var clients = JsonConvert.DeserializeObject<IEnumerable<Client>>(content);
            Clients.Clear();
            foreach (var client in clients)
            {
                Clients.Add(client);
            }
        }
        else {
            MessageBox.Show("Server is not responding", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }

    private async void AddClient()
    {
        var detailsWindow = new ClientDetailsWindow { Client = new Client() };
        if (detailsWindow.ShowDialog() == true)
        {
            var client = detailsWindow.Client;
            var json = JsonConvert.SerializeObject(client);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(BaseUrl, content);
            
            GetClients();
            if(!response.IsSuccessStatusCode) {
                MessageBox.Show("Something went wrong, check if the data is correct and try again", "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }

    private async void DeleteClient()
    {
        if (SelectedClientId <= 0) return;

        var response = await _httpClient.DeleteAsync($"{BaseUrl}/{SelectedClientId}");
        GetClients();
        if(!response.IsSuccessStatusCode) {
            MessageBox.Show("Something went wrong, check if the data is correct and try again", "Error",
                MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }

    private async void UpdateClient()
    {
        var selectedClient = Clients.FirstOrDefault(c => c.Id == SelectedClientId);
        if (selectedClient != null)
        {
            var detailsWindow = new ClientDetailsWindow { Client = selectedClient };
            if (detailsWindow.ShowDialog() == true)
            {
                var updatedClient = detailsWindow.Client;
                var json = JsonConvert.SerializeObject(updatedClient);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _httpClient.PutAsync($"{BaseUrl}/{SelectedClientId}", content);
                GetClients();
                if(!response.IsSuccessStatusCode) {
                    MessageBox.Show("Something went wrong, check if the data is correct and try again", "Error",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}