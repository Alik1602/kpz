﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Windows;
using GalaSoft.MvvmLight.CommandWpf;
using Newtonsoft.Json;
using WpfApp2.Models;

namespace WpfApp2.ViewModels; 

public class TeamViewModel {
    public ObservableCollection<Team> Teams { get; set; } = null!;
    public int SelectedTeamId { get; set; }
    public RelayCommand GetCommand { get; set; } = null!;
    public RelayCommand AddCommand { get; set; } = null!;
    public RelayCommand UpdateCommand { get; set; } = null!;
    public RelayCommand DeleteCommand { get; set; } = null!;
    
    private readonly HttpClient _httpClient;
    private const string BaseUrl = "http://localhost:5117/api/teams";
    
    public TeamViewModel()
    {
        _httpClient = new HttpClient();
        Teams = new ObservableCollection<Team>();
        GetCommand = new RelayCommand(GetTeams);
        AddCommand = new RelayCommand(AddTeam);
        UpdateCommand = new RelayCommand(UpdateTeam);
        DeleteCommand = new RelayCommand(DeleteTeam);
    }
    
    public async void GetTeams()
    {
        var response = await _httpClient.GetAsync(BaseUrl);
        if (response.IsSuccessStatusCode)
        {
            var content = await response.Content.ReadAsStringAsync();
            var teams = JsonConvert.DeserializeObject<IEnumerable<Team>>(content);
            Teams.Clear();
            foreach (var team in teams)
            {
                Teams.Add(team);
            }
        }
        else {
            MessageBox.Show("Server is not responding", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
    
    private async void AddTeam()
    {
        var detailsWindow = new TeamDetailsWindow { Team = new Team() };
        if (detailsWindow.ShowDialog() == true)
        {
            var team = detailsWindow.Team;
            var json = JsonConvert.SerializeObject(team);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(BaseUrl, content);
            GetTeams();
            if(!response.IsSuccessStatusCode) {
                MessageBox.Show("Something went wrong, check if the data is correct and try again", "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
    
    private async void UpdateTeam()
    {
        var detailsWindow = new TeamDetailsWindow { Team = Teams.FirstOrDefault(t => t.Id == SelectedTeamId) };
        if (detailsWindow.ShowDialog() == true)
        {
            var team = detailsWindow.Team;
            var json = JsonConvert.SerializeObject(team);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PutAsync(BaseUrl + "/" + team.Id, content);
            GetTeams();
            if(!response.IsSuccessStatusCode) {
                MessageBox.Show("Something went wrong, check if the data is correct and try again", "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
    
    private async void DeleteTeam()
    {
        if (SelectedTeamId <= 0) return;
        
        var response = await _httpClient.DeleteAsync($"{BaseUrl}/{SelectedTeamId}");
        GetTeams();
        if(!response.IsSuccessStatusCode) {
            MessageBox.Show("Something went wrong, check if the data is correct and try again", "Error",
                MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}