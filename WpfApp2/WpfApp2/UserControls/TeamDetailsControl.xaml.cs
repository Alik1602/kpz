﻿using System.Windows;
using System.Windows.Controls;
using WpfApp2.Models;

namespace WpfApp2.UserControls; 

public partial class TeamDetailsControl : UserControl {
    public static readonly DependencyProperty TeamProperty = DependencyProperty.Register(
        "Team", typeof(Team), typeof(TeamDetailsControl), new PropertyMetadata(null));

    public Team Team
    {
        get { return (Team)GetValue(TeamProperty); }
        set { SetValue(TeamProperty, value); }
    }

    public TeamDetailsControl()
    {
        InitializeComponent();
    }
}