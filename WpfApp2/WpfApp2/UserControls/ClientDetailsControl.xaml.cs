﻿using System.Windows;
using System.Windows.Controls;
using WpfApp2.Models;

namespace WpfApp2.UserControls; 

public partial class ClientDetailsControl : UserControl {
    public static readonly DependencyProperty ClientProperty = DependencyProperty.Register(
        "Client", typeof(Client), typeof(ClientDetailsControl), new PropertyMetadata(null));

    public Client Client
    {
        get { return (Client)GetValue(ClientProperty); }
        set { SetValue(ClientProperty, value); }
    }

    public ClientDetailsControl()
    {
        InitializeComponent();
    }
}