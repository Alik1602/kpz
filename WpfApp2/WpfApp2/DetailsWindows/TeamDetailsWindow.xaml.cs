﻿using System.Windows;
using WpfApp2.Models;

namespace WpfApp2; 

public partial class TeamDetailsWindow : Window {
    public static readonly DependencyProperty ClientProperty = DependencyProperty.Register(
        "Team", typeof(Team), typeof(ClientDetailsWindow), new PropertyMetadata(null));

    public Team Team
    {
        get { return (Team)GetValue(ClientProperty); }
        set { SetValue(ClientProperty, value); }
    }

    public TeamDetailsWindow()
    {
        InitializeComponent();
        DataContext = this;
    }

    private void SaveButton_Click(object sender, RoutedEventArgs e)
    {
        this.DialogResult = true;
        this.Close();
    }

    private void CancelButton_Click(object sender, RoutedEventArgs e)
    {
        this.DialogResult = false;
        this.Close();
    }
}