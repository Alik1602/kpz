﻿using System.Windows;
using WpfApp2.Models;

namespace WpfApp2; 

public partial class ClientDetailsWindow : Window {
    public static readonly DependencyProperty ClientProperty = DependencyProperty.Register(
        "Client", typeof(Client), typeof(ClientDetailsWindow), new PropertyMetadata(null));

    public Client Client
    {
        get { return (Client)GetValue(ClientProperty); }
        set { SetValue(ClientProperty, value); }
    }

    public ClientDetailsWindow()
    {
        InitializeComponent();
        DataContext = this;
    }

    private void SaveButton_Click(object sender, RoutedEventArgs e)
    {
        this.DialogResult = true;
        this.Close();
    }

    private void CancelButton_Click(object sender, RoutedEventArgs e)
    {
        this.DialogResult = false;
        this.Close();
    }
}