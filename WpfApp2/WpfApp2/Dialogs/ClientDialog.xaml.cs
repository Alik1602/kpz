﻿using System;
using System.Windows;

namespace WpfApp2.Dialogs; 

public partial class ClientDialog : Window {
    public int ClientId { get; set; }
    
    public ClientDialog() {
        InitializeComponent();
    }
    
    private void OkButton_Click(object sender, RoutedEventArgs e)
    {
        ClientId = Convert.ToInt32(ClientIdTextBox.Text);
        this.DialogResult = true;
    }
}