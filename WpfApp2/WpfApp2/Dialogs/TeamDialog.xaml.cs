﻿using System;
using System.Windows;

namespace WpfApp2.Dialogs; 

public partial class TeamDialog : Window {
    public int TeamId { get; set; }
    
    public TeamDialog() {
        InitializeComponent();
    }
    
    private void OkButton_Click(object sender, RoutedEventArgs e)
    {
        TeamId = Convert.ToInt32(TeamIdTextBox.Text);
        this.DialogResult = true;
    }
}