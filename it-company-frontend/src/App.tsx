import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Sidebar from './components/Sidebar';
import Clients from './pages/Clients';
import Teams from './pages/Team';

function App() {
  return (
    <>
      <BrowserRouter>
        <div className="flex h-screen">
          <Sidebar />
          <div className="flex-grow overflow-auto bg-slate-400">
            <Routes>
              <Route path="/" element={<Clients />} />
              <Route path="/teams" element={<Teams />} />
            </Routes>
          </div>
        </div>
      </BrowserRouter>
    </>
  );
}

export default App;
