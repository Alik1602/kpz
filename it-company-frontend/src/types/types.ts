export type Team = {
  id: number
  name: string
};

export type Project = {
  id: number
  name: string
  description: string
  teamId: number
  clientId: number
};

export type Client = {
  id: number
  name: string
  contactPerson: string
  email: string
  phoneNumber: string
};

