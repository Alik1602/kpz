const SERVER_URL = 'http://localhost:5117/api/';

type httpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';

export class HttpError extends Error {
  code: number | undefined;
  constructor(message: string, code?: number) {
    super(message);
    if (code) {
      this.code = code;
    }
    this.name = 'HttpError';
  }
}

async function httpRequest<T>(
  endpoint: string,
  method: httpMethod = 'GET',
  data?: unknown
): Promise<T | Record<string, unknown>> {
  const config: RequestInit = {
    method,
    headers: {
      'Content-Type': 'application/json'
    }
  };

  if (data) {
    config.body = JSON.stringify(data);
  }

  let response: Response | undefined;
  try {
    response = await fetch(SERVER_URL + endpoint, config);
    if (!response.ok) {
      throw new HttpError(response.statusText, response.status);
    }
    if (response.status === 204) return {};
    const responseBody = await response.json();
    return responseBody;
  } catch (error) {
    if (error instanceof HttpError) {
      throw error;
    }
    if (response) {
      throw new HttpError(response?.statusText, response?.status);
    } else {
      throw new HttpError('Something went wrong');
    }
  }
}

export default httpRequest;
