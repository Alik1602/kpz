/* eslint-disable max-len */
import { useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

const Sidebar = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [currentPage, setCurrentPage] = useState(location.pathname);

  const isActive = (path: string) => currentPage === path;

  return (
    <div className="h-screen w-32 bg-slate-50 shadow-md">
      <div className="flex flex-col p-4">
        <button
          onClick={() => { setCurrentPage('/'); navigate('/'); }}
          className={`text-left w-full py-2 px-4 mb-2 rounded-md ${isActive('/') ? 'bg-blue-600 text-white' : 'text-black'}`}
        >
          Client
        </button>
        <button
          onClick={() => { setCurrentPage('/teams'); navigate('/teams'); }}
          className={`text-left w-full py-2 px-4 mb-2 rounded-md ${isActive('/teams') ? 'bg-blue-600 text-white' : 'text-black'}`}
        >
          Team
        </button>
      </div>
    </div>
  );
};

export default Sidebar;
