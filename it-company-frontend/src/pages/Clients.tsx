import { useEffect, useState } from 'react';
import httpRequest, { type HttpError } from '../utils/utils';
import { type Client } from '../types/types';

const Clients = () => {
  const [clients, setClients] = useState<Client[]>([]);
  const [clientId, setClientId] = useState<number>(0); // For updating existing client
  const [clientName, setClientName] = useState(''); // For creating new client
  const [clientContactPerson, setClientContactPerson] = useState(''); // For creating new client
  const [clientEmail, setClientEmail] = useState(''); // For creating new client
  const [clientPhone, setClientPhone] = useState(''); // For creating new client

  const fetchClients = (success: boolean) => {
    httpRequest<Client[]>('clients').then((data) => {
      setClients(data as Client[]);
      if (success) {
        console.log('clients fetched successfully');
      }
    }).catch((e: HttpError) => {
      if (e.code) {
        console.error(`${e.code} ${e.message}`);
      } else {
        console.error(e.message);
      }
    });
  };

  useEffect(() => {
    fetchClients(true);
  }, []);

  const handleDelete = (id: number) => {
    httpRequest<Client>(`clients/${id}`, 'DELETE')
      .then(() => {
        console.log('Client deleted successfully');
        fetchClients(false); // Re-fetch clients after successful operation
      })
      .catch((e: HttpError) => {
        if (e.code) {
          console.error(`${e.code} ${e.message}`);
        } else {
          console.error(e.message);
        }
      });
  };

  const handleCreateOrUpdate = () => {
    if (!clientName || !clientContactPerson || !clientEmail || !clientPhone) {
      console.error('Please enter all the details');
      return;
    }

    const client: Client = {
      id: clientId,
      name: clientName,
      contactPerson: clientContactPerson,
      email: clientEmail,
      phoneNumber: clientPhone
    };

    const operation = client.id ? 'PUT' : 'POST';
    const endpoint = client.id ? `clients/${client.id}` : 'clients';

    httpRequest<Client>(endpoint, operation, client)
      .then(() => {
        console.log(`Client ${client.id ? 'updated' : 'created'} successfully`);
        fetchClients(false); // Re-fetch clients after successful operation
      })
      .catch((e: HttpError) => {
        if (e.code) {
          console.error(`${e.code} ${e.message}`);
        } else {
          console.error(e.message);
        }
      });

    if (clientId !== 0) {
      setClientId(0);
      setClientName('');
      setClientContactPerson('');
      setClientEmail('');
      setClientPhone('');
    }
  };

  const handleUpdate = (client: Client) => {
    setClientId(client.id);
    setClientName(client.name);
    setClientContactPerson(client.contactPerson);
    setClientEmail(client.email);
    setClientPhone(client.phoneNumber);
  };

  return (
    <div className="p-4">
      <h1 className="text-2xl font-bold">Clients</h1>
      <div className='flex justify-start gap-4 py-4'>
        <input
          type="text"
          placeholder="Client Name"
          value={clientName}
          onChange={(e) => setClientName(e.target.value)}
          className="px-3 py-2 border rounded-md"
        />
        <input
          type="text"
          placeholder=""
          value={clientContactPerson}
          onChange={(e) => setClientContactPerson(e.target.value)}
          className=" px-3 py-2 border rounded-md"
        />
        <input
          type="text"
          placeholder=""
          value={clientEmail}
          onChange={(e) => setClientEmail(e.target.value)}
          className=" px-3 py-2 border rounded-md"
        />
        <input
          type="text"
          placeholder=""
          value={clientPhone}
          onChange={(e) => setClientPhone(e.target.value)}
          className=" px-3 py-2 border rounded-md"
        />
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-5 rounded"
          onClick={handleCreateOrUpdate}
        >
          {clientId ? 'Update Client' : 'Create Client'}
        </button>
      </div>
      <div className="flex gap-4 items-center">
        <table className="min-w-[40rem] bg-white">
          <thead className="bg-blue-600 text-white">
            <tr>
              <th className="text-center py-3 px-4 uppercase font-semibold text-sm">ID</th>
              <th className="text-center py-3 px-4 uppercase font-semibold text-sm">Name</th>
              <th className="text-center py-3 px-4 uppercase font-semibold text-sm">Contact Person</th>
              <th className="text-center py-3 px-4 uppercase font-semibold text-sm">Email</th>
              <th className="text-center py-3 px-4 uppercase font-semibold text-sm">Phone</th>
              <th className="text-center py-3 px-4 uppercase font-semibold text-sm">Actions</th>
            </tr>
          </thead>
          <tbody className="text-gray-700">
            {clients.map((client) => (
              <tr key={client.id}>
                <td className="text-center py-3 px-4">{client.id}</td>
                <td className="text-center py-3 px-4">{client.name}</td>
                <td className="text-center py-3 px-4">{client.contactPerson}</td>
                <td className="text-center py-3 px-4">{client.email}</td>
                <td className="text-center py-3 px-4">{client.phoneNumber}</td>
                <td className="flex justify-center items-center text-center py-3 px-2">
                  <button
                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-3 mr-2 rounded"
                    onClick={() => handleUpdate(client)}
                  >
                    Update
                  </button>
                  <button
                    className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-3 rounded"
                    onClick={() => handleDelete(client.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Clients;
