import { useEffect, useState } from 'react';
import httpRequest, { type HttpError } from '../utils/utils';
import { type Team } from '../types/types';

const Teams = () => {
  const [team, setStations] = useState<Team[]>([]);
  const [teamId, setTeamId] = useState<number>(0);
  const [teamName, setTeamName] = useState('');

  const fetchTeams = (success: boolean) => {
    httpRequest<Team[]>('teams').then((data) => {
      setStations(data as Team[]);
      if (success) {
        console.log('teams fetched successfully');
      }
    }).catch((e: HttpError) => {
      if (e.code) {
        console.error(`${e.code} ${e.message}`);
      } else {
        console.error(e.message);
      }
    });
  };

  useEffect(() => {
    fetchTeams(true);
  }, []);

  const handleDelete = (id: number) => {
    httpRequest<Team>(`teams/${id}`, 'DELETE')
      .then(() => {
        console.log('Team deleted successfully');
        fetchTeams(false); // Re-fetch team after successful operation
      })
      .catch((e: HttpError) => {
        if (e.code) {
          console.error(`${e.code} ${e.message}`);
        } else {
          console.error(e.message);
        }
      });

    if (teamId !== 0) {
      setTeamId(0);
      setTeamName('');
    }
  };

  const handleCreateOrUpdate = () => {
    if (!teamName) {
      console.error('Please enter all the details');
      return;
    }

    const team: Team = {
      id: teamId,
      name: teamName
    };

    const operation = team.id ? 'PUT' : 'POST';
    const endpoint = team.id ? `teams/${team.id}` : 'teams';

    httpRequest<Team>(endpoint, operation, team)
      .then(() => {
        console.log(`Team ${team.id ? 'updated' : 'created'} successfully`);
        fetchTeams(false); // Re-fetch team after successful operation
      })
      .catch((e: HttpError) => {
        if (e.code) {
          console.error(`${e.code} ${e.message}`);
        } else {
          console.error(e.message);
        }
      });

    setTeamId(0);
    setTeamName('');
  };

  const handleUpdate = (team: Team) => {
    setTeamId(team.id);
    setTeamName(team.name);
  };

  return (
    <div className="p-4">
      <h1 className="text-2xl font-bold">Teams</h1>
      <div className='flex justify-start gap-4 py-4'>
        <input
          type="text"
          placeholder="Team name"
          value={teamName}
          onChange={(e) => setTeamName(e.target.value)}
          className="px-3 py-2 border rounded-md"
        />
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-5 rounded"
          onClick={handleCreateOrUpdate}
        >
          {teamId ? 'Update Team' : 'Create Team'}
        </button>
      </div>
      <div className="flex gap-4 items-center">
        <table className="min-w-[40rem] bg-white">
          <thead className="bg-blue-600 text-white">
            <tr>
              <th className="text-center py-3 px-4 uppercase font-semibold text-sm">ID</th>
              <th className="text-center py-3 px-4 uppercase font-semibold text-sm">Name</th>
              <th className="text-center py-3 px-4 uppercase font-semibold text-sm">Actions</th>
            </tr>
          </thead>
          <tbody className="text-gray-700">
            {team.map((team) => (
              <tr key={team.id}>
                <td className="text-center py-3 px-4">{team.id}</td>
                <td className="text-center py-3 px-4">{team.name}</td>
                <td className="flex justify-center items-center text-center py-3 px-2">
                  <button
                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-3 mr-2 rounded"
                    onClick={() => handleUpdate(team)}
                  >
                    Update
                  </button>
                  <button
                    className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-3 rounded"
                    onClick={() => handleDelete(team.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Teams;
